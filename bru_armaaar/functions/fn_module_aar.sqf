//Argument 0 is module logic
_logic = param [0,objNull,[objNull]];
//Argument 1 is list of affected units
_units = param [1,[],[[]]];
//Argument 2 is true when activates, false when it's deactivated (this one should always be true);
_activated = param [2,true,[true]];

if (_activated) then {
	_freq = _logic getVariable ["Frequency", -1];
	BRU_fnc_logPosition = compile preProcessFileLineNumbers "\bru_armaaar\functions\fn_logPosition.sqf";
	diag_log format ["Logging %1 positions to file every %2 seconds (%3 minutes)", count _units, _freq, _freq/60];
	while {true} do {
		//player globalChat format ["Frequency is: %1", _freq];
		{
			[_x] spawn BRU_fnc_logPosition;
		} forEach _units;
		sleep _freq;
	};
};

true