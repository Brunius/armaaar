class CfgPatches {
	class bru_armaaar {
		units[] = {"bru_module_aar"};
		author = "Brunius";
		requiredVersion = 1.0;
		requiredAddons[] = {"A3_Modules_F"};
	};
};

class CfgFactionClasses {
	class bru;
	class bru_moduleClass : bru
	{
		displayName = "BRU - Modules";
	};
};

class CfgVehicles {
	class Logic;
	class Module_F: Logic {
		class AttributesBase {
			class Default;
			class Edit;
			class Combo;
			class Checkbox;
			class CheckboxNumber;
			class ModuleDescription;
			class Units;
		};
		
		class ModuleDescription {
			class AnyBrain;
		};
	};
	
	class bru_module_aar : Module_F
	{
		scope = 2;
		displayName = "BRU AAR Module";
		//icon = "";
		category = "Multiplayer";
		function = "bru_fnc_module_aar";
		functionPriority = 1;
		isGlobal = 1;				//Set to 0 for production - 1 is on every client, 0 is server only
		isTriggerActivated = 0;		//Will not wait for any attached triggers
		is3DEN = 0;					//Do not run init function in editor
		author = "Brunius";
		
		curatorInfoType = "RscDisplayAttributeModuleAAR";	//Not sure what this does, but the biki says it's needed
		
		class Attributes: AttributesBase {
			class Units: Units {
				property = "bru_module_aar_Units";
			};
			
			class Frequency : Combo {
				property = "bru_module_aar_freq";
				displayName = "Frequency";
				description = "Frequency of positional logging.";
				typeName = "NUMBER";
				defaultValue = "10";
				class Values {
					class s10		{name = "10 seconds"; value = 10;};
					class s30		{name = "30 seconds"; value = 30;};
					class s60		{name = "60 seconds"; value = 60;};
					class m2		{name = "2 minutes"; value = 120;};
					class m3		{name = "3 minutes"; value = 180;};
					class m4		{name = "4 minutes"; value = 240;};
					class m5		{name = "5 minutes"; value = 300;};
					class m10		{name = "10 minutes"; value = 600;};
				};
			};
			
			class Name: Edit {
				displayName = "Name";
				description = "Name to be attached to the output file.";
				typeName = "STRING";
				defaultValue = """""";
			};
			class ModuleDescription : ModuleDescription{};
		};
		
		class ModuleDescription : ModuleDescription {
			description = "This module enables the positional logging in the AAR mod. This is logged to a file named with date, time, and mission name.";
		};
	};
};

class CfgFunctions {
	class bru {
		tag = "BRU";
		class Multiplayer {
			file = "\bru_armaaar\functions";
			class module_aar{};
		};
	};
};